/**
 * @author Alper GULEC - Test Automation Engineer
 * In addition, allure test integration was performed.
 * The test outputs the report after the run.
 * "allure serve allure-results" is written in the terminal and the report can be viewed in html format.
 * Using slf4j, logging was performed at error and info level.
 * "chrome driver.exe " is read in the project.
 */

package ciceksepeti.web.test;

import base.TestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.WelcomePage;
import pages.SignInPage;

public class TestCases extends BaseTest {


    @Test(priority = 5)
    @Description("Succesfully sign-in")
    @Severity(SeverityLevel.BLOCKER)
    public void successSignIn() throws InterruptedException {

        SignInPage signInPage = new SignInPage(driver, wait);
        HomePage homePage = new HomePage(driver, wait);
        signInPage.userFillInTheEmailField(TestBase.prop().getProperty("email"));
        signInPage.userFillInThePasswordField(TestBase.prop().getProperty("password"));
        signInPage.clickSıgnInButn();
        homePage.checkMyAccountText();
    }

    @Test(priority = 4)
    @Description("Unsuccesfully sign-in with incorrect password")
    @Severity(SeverityLevel.BLOCKER)
    public void unSuccesfullySıgnIn() throws InterruptedException {

        SignInPage signInPage = new SignInPage(driver, wait);
        signInPage.userFillInTheEmailField(TestBase.prop().getProperty("email")); // The email is received from the config.properties file.
        signInPage.userFillInThePasswordField(TestBase.prop().getProperty("inCorrect")); // The inCorrect password is received from the config.properties file.
        signInPage.clickSıgnInButn();
        signInPage.verifyOpenedPopUp();

    }

    @Test(priority = 3)
    @Description("The user must enter a valid email address.")
    @Severity(SeverityLevel.BLOCKER)
    public void sıgnInWıthInvalidEmail() throws InterruptedException {

        SignInPage signInPage = new SignInPage(driver, wait);
        signInPage.userFillInTheEmailField(TestBase.prop().getProperty("invalidEmail")); // The invalidEmail is received from the config.properties file.
        signInPage.userFillInThePasswordField(TestBase.prop().getProperty("password")); //  The password is received from the config.properties file.
        signInPage.verifyErrorMessageForInvalidEmail();

    }

    @Test(priority = 2)
    @Description("if the user does not fill in the required fields 'Required field.' is displayed under the e-mail and password fields")
    @Severity(SeverityLevel.BLOCKER)
    public void unSuccesfullySignInBlankFields()  {

        SignInPage signInPage = new SignInPage(driver, wait);
        signInPage.clickSıgnInButn();
        signInPage.requiredFieldCheck();


    }

    @Test(priority = 1)
    @Description("Unsuccesfully forgot password with wrong e-mail address")
    @Severity(SeverityLevel.BLOCKER)
    public void unSuccesfullyForgotPassword() throws InterruptedException {

        WelcomePage welcomePage = new WelcomePage(driver, wait);
        SignInPage signInPage = new SignInPage(driver,wait);
        welcomePage.goToLolaflora();
        welcomePage.checkPopUpOnHomePage();
        welcomePage.clickSignInButton();
        signInPage.forgotPassword();
        signInPage.fillEmailAdress();
        signInPage.clickSendButton();
        signInPage.verifyemailAdressNotFoundPopUp();
    }

}
