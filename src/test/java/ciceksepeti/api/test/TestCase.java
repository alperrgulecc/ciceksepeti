/**
 * @author Alper GULEC - Test Automation Engineer
 * In addition, allure test integration was performed.
 * The test outputs the report after the run.
 * "allure serve allure-results" is written in the terminal and the report can be viewed in html format.
 * Using slf4j, logging was performed at error and info level.
 * "chrome driver.exe " is read in the project.
 */

package ciceksepeti.api.test;

import groovy.util.logging.Slf4j;
import io.qameta.allure.Description;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

import static io.restassured.RestAssured.baseURI;

@Slf4j
public class TestCase {

    private static final Logger log = LoggerFactory.getLogger(TestCase.class);
    private List<String> installment;

    public io.restassured.specification.RequestSpecification requestSpecification() {
        baseURI = "https://830c4c91-5308-404f-836a-4899a0c63d27.mock.pstmn.io";
        RequestSpecification requestSpecification = RestAssured.given();
        requestSpecification.header("Accept", "application/json");
        requestSpecification.header("Content-Type", "application/json");
        return requestSpecification;
    }

    public void cicekSepeti(String installment) throws JSONException {

        RequestSpecification requestSpecification = requestSpecification();
        requestSpecification.queryParam("installment", installment);

        Response response = requestSpecification.get("/test");

        int statusCode = response.getStatusCode();

        if (installment.equals("0")) { // installment 0 queryParams'da taksit seçeneği olmamalı olarak belirtilmişti. Ancak taksit seçeneği olan ürünler olduğu için hata alınmaktadır.

            JSONArray jsonArray = getJsonArray(response);

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject finalObject = jsonArray.getJSONObject(i);
                boolean value = finalObject.getBoolean("installment");
                String code = finalObject.getString("code");

                if (value != false){

                    log.error(" [ ERROR ] [ WHEN INSTALLMENT PARAMS 0 IS SENT, THERE SHOULD BE NO INSTALLMENT OPTION IN THE RESPONSE ] [ ERROR PRODUCT INDEX ] : {} [ CODE OF ERROR PRODUCT ]",i,code);
                }
                Assert.assertFalse(value);


            }

        } else if (installment.equals("1")) {


            JSONArray jsonArray = getJsonArray(response);

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject finalObject = jsonArray.getJSONObject(i);
                boolean value = finalObject.getBoolean("installment");
                Assert.assertTrue(value);

            }

        } else if (installment.equals("null")) {

            Assert.assertEquals(500, statusCode);
        }


    }

    private JSONArray getJsonArray(Response response) throws JSONException {

        JSONObject parentObject = new JSONObject(response.asString());
        JSONObject pageName1 = parentObject.getJSONObject("result");
        JSONObject pageN = pageName1.getJSONObject("data");
        return pageN.getJSONArray("products");
    }

    @Test(priority = 1)
    @Description("Get api with 0 installment params")
    public void installment0() throws JSONException {
        cicekSepeti("0");
    }

    @Test(priority = 2)
    @Description("Get api with 1 installment params")
    public void installment1() throws JSONException {
        cicekSepeti("1");
    }

    @Test(priority = 3)
    @Description("Get api with null installment params")
    public void installmentNull() throws JSONException {
        cicekSepeti("null");
    }
}
