package pages;

import base.TestBase;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignInPage extends TestBase {

    public SignInPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    private final static String emailFieldId = "//input[@id='EmailLogin']";
    private final static String passwordFieldId = "//input[@id='Password']";
    private final static String signInButton = "//button[@class='btn btn-primary btn-lg pull-right login__btn js-login-button']";
    private final static String verifyPopUp = "//div[@class='modal-body']";
    private final static String verifyInvalidMessageForInvalidEmail = "//span[@id='EmailLogin-error']";
    private final static String closeVerifyInvalidMessageForInvalidEmail = "//button[@class='btn btn-primary']";
    private final static String requiredFielsMessage = "//span[@id='EmailLogin-error']";
    private final static String forgotPasswordButton = "//a[@class='login__forgot-password js-forgot-password']";
    private final static String forgotPasswordEmailField = "//input[@id='Mail']";
    private final static String sendButton = "//button[@class='btn btn-lg btn-primary form-password-recovery__btn js-password-recovery-button']";
    private final static String popUpEmailAdressNotFound = "//div[@class='modal-body']";
    private final static String closePopUpEmailAdressNotFound = "//button[@class='btn btn-primary']";

    @Step("User fill email field with : {0}")
    public void userFillInTheEmailField(String email) {

        driver.findElement(By.xpath(emailFieldId)).clear();
        driver.findElement(By.xpath(emailFieldId)).sendKeys(email);

    }

    @Step("User fill password field with : {0}")
    public void userFillInThePasswordField(String password) {

        driver.findElement(By.xpath(passwordFieldId)).clear();
        driver.findElement(By.xpath(passwordFieldId)).sendKeys(password);

    }

    @Step("User click sign in button")
    public void clickSıgnInButn() {

        driver.findElement(By.xpath(signInButton)).click();
    }

    @Step("Verify that 'E-mail or password is incorrect.Please check your information and try again.' pop-up open")
    public void verifyOpenedPopUp() throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(By.xpath(verifyPopUp)).isDisplayed();
        driver.findElement(By.xpath(closeVerifyInvalidMessageForInvalidEmail)).click();
    }

    @Step("Verify if inline error message 'Please enter a valid e-mail address.' is displayed under the e-mail field")
    public void verifyErrorMessageForInvalidEmail() throws InterruptedException {

        driver.findElement(By.xpath(verifyInvalidMessageForInvalidEmail)).isDisplayed();

    }

    @Step("if the user does not fill in the required fields 'Required field.' is displayed under the e-mail and password fields")
    public void requiredFieldCheck() {

        driver.findElement(By.xpath(requiredFielsMessage)).isDisplayed();
    }

    @Step("User click 'Forgot Password'")
    public void forgotPassword() {

        driver.findElement(By.xpath(forgotPasswordButton)).click();
    }

    @Step("User fill email adress")
    public void fillEmailAdress() {

        driver.findElement(By.xpath(forgotPasswordEmailField)).sendKeys(TestBase.prop().getProperty("notSignedEmail"));
    }

    @Step("User clik 'Send' button")
    public void clickSendButton() {

        driver.findElement(By.xpath(sendButton)).click();
    }

    @Step("User verify that 'E-mail Address not Found' pop-up open")
    public void verifyemailAdressNotFoundPopUp() throws InterruptedException {

        driver.findElement(By.xpath(popUpEmailAdressNotFound)).isDisplayed();
        Thread.sleep(1000);
        driver.findElement(By.xpath(closePopUpEmailAdressNotFound)).click();
    }
}

