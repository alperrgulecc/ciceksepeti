package pages;

import base.TestBase;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WelcomePage extends TestBase {

    private final static String signInButton = "//div[@class='container']//li[@class='user-menu__item js-toggle-process-link']//li[1]//a[1]";
    private final static String signInMenu = "//div[@class='header__right-col']//a[@class='user-menu__link user-process-toggle']";
    private final static String closeİcon = "";

    public WelcomePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @Step("If the pop-up is opened on the home page, it closes")
    public void checkPopUpOnHomePage() {
        try {
             driver.findElement(By.xpath("//span[@class='icon-close']")).click();
        } catch (NoSuchElementException ignored) {
        }
    }

    @Step("User open lolaflora.com")
    public void goToLolaflora() {

        driver.get(prop().getProperty("url"));
    }

    @Step("User click sign-in button on homepage")
    public void clickSignInButton() throws InterruptedException {

        Actions actions = new Actions(driver);
        WebElement webElement = driver.findElement(By.xpath(signInMenu));
        actions.moveToElement(webElement);
        Thread.sleep(2000);
        WebElement webElement1 = driver.findElement(By.xpath(signInButton));
        actions.moveToElement(webElement1).click().build().perform();

    }


}
