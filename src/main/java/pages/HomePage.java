package pages;

import base.TestBase;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class HomePage extends TestBase {

    public HomePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    private final static String myAccount = "//div[@class='header__right-col']//span[@class='user-menu__title'][contains(text(),'My Account')]";

    @Step(" The user checks that he has successfully logged in.")
    public void checkMyAccountText() throws InterruptedException {

        Thread.sleep(1000);
        String text = driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[1]/div[2]/div[2]/nav[1]/ul[1]/li[2]/a[1]/span[2]")).getText();
        Assert.assertEquals("My Account",text);


    }
}
